@extends('master')
@section('content')
    <form action="{{url('/cadastrar')}}" method="POST">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="row">
            <div class="col-sm-12 form-group">
                <label for="nomeMedico">Nome</label>
                <input class="form-control" type="text" id="nomeMedico" name="nomeMedico">
            </div>
            <div class="col-sm-12 form-group">
                <label for="crmMedico">CRM</label>
                <input class="form-control" type="text" id="crmMedico" name="crmMedico">
            </div>
            <div class="col-sm-12 form-group">
                <label for="uf">UF</label>
                <input class="form-control" type="text" id="uf" name="uf">
            </div>
            <div class="col-sm-12 form-group">
                <label for="especialidade">Especialidade</label>
                <input class="form-control" type="text" id="especialidade" name="especialidade">
            </div>
        </div>
        <button type="submit" name="btnCadastra" class="btn btn-primary">Cadastrar</button>
    </form>
@endsection