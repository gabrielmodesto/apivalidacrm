@extends('master')
@section('content')
    <!-- Inicio do formulario -->
    <form method="get" action="">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <fieldset disabled>
            <div class="form-group">
                <label for="exampleInputEmail1">CRM</label>
                <input value="{{$CRM}}" type="text" class="form-control" id="exampleInputEmail1" name="CRM">
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Estado</label>
                <input value="{{$UF}}" type="text" class="form-control" id="exampleInputPassword1" name="UF">
            </div>
            <div class="form-group">
                <label for="exampleInputName">Nome</label>
                <input value="{{$nome}}" type="text" class="form-control" id="exampleInputName" name="NOME">
            </div>
            <div class="form-group">
                <label for="exampleInputSituacao">Situação</label>
                <input value="{{$status}}" type="text" class="form-control" id="exampleInputSituacao" name="SITUACAO">
            </div>
            <div class="form-group">
                <label for="exampleInputSituacao">Profissão</label>
                <input value="{{$especialidade}}" type="text" class="form-control" id="exampleInputSituacao" name="PROFISSAO">
            </div>
        </fieldset>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection