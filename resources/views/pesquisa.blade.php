@extends('master')
@section('content')
    <!-- Inicio do formulario -->
    <form method="POST" action="{{url('/confirma')}}">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="form-group">
            <label for="exampleInputCrm">CRM</label>
            <input type="text" class="form-control" id="exampleInputCrm" name="CRM">
        </div>
        <div class="form-group">
            <label for="exampleInputUf">Estado</label>
            <input type="text" class="form-control" id="exampleInputUf" name="UF">
        </div>
        <fieldset disabled>
            <div class="form-group">
                <label for="exampleInputName">Nome</label>
                <input type="text" class="form-control" id="exampleInputName" name="NOME">
            </div>
            <div class="form-group">
                <label for="exampleInputSituacao">Situação</label>
                <input type="text" class="form-control" id="exampleInputSituacao" name="SITUACAO">
            </div>
            <div class="form-group">
                <label for="exampleInputSituacao">Profissão</label>
                <input type="text" class="form-control" id="exampleInputSituacao" name="PROFISSAO">
            </div>
        </fieldset>
        <button type="submit" name="send" id="ButtomCheck" class="btn btn-primary">Check</button>
    </form>
@endsection
