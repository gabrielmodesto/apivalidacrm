<?php

Route::get('/pesquisa', function (){
    return view('pesquisa');
});
Route::post('/confirma', 'PesquisaController@confirma');
Route::get('/cadastro', 'CadastroMedicoController@create');
Route::post('/cadastrar', 'CadastroMedicoController@store');
Route::get('/', function () {
    return view('welcome');
});
