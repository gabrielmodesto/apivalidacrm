<?php

namespace App\Http\Controllers;


class PesquisaController extends Controller
{
    public function index(){
        return view('pesquisa');
    }

    public function confirma(){
        $UF = $_POST['UF'];
        $CRM = $_POST['CRM'];

        $nome = "";
        $status = "";

        $url = 'https://www.consultacrm.com.br/api/index.php?tipo=crm&uf='.$UF.'&q='.$CRM.'&chave=7090803883&destino=json';

        $dadosSite = file_get_contents($url);
        $var1 = explode('"nome":"', $dadosSite);
        $var2 = explode('",', $var1[1]);

        $var3 = explode('"profissao":"', $dadosSite);
        $var4 = explode('",', $var3[1]);

        $var7 = explode('"uf":"', $dadosSite);
        $var8 = explode('",', $var7[1]);

        $var5 = explode('"situacao":"', $dadosSite);
        $var6 = explode('"}]}', $var5[1]);

        $nome = $var2[0];
        $profi = $var4[0];
        $status = $var6[0];

        if($profi == ''){
            $profi = 'Não há Registro';
        }
        if($status != 'Ativo'){
            echo "<script>alert('Não está ativo');</script>";
        }
        return view('confirma', ['CRM' => $CRM, 'UF' => $UF, 'nome' => $nome, 'status' => $status, 'especialidade' => $profi]);
    }

}
